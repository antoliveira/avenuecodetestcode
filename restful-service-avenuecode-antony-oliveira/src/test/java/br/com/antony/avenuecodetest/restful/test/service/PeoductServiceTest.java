package br.com.antony.avenuecodetest.restful.test.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.antony.avenuecodetest.restful.model.ImageModel;
import br.com.antony.avenuecodetest.restful.model.ProductModel;
import br.com.antony.avenuecodetest.restful.service.DefaultProductService;
import br.com.antony.avenuecodetest.restful.service.ProductService;

@RunWith(SpringJUnit4ClassRunner.class )
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ContextConfiguration
public class PeoductServiceTest {

	@Autowired
	private DefaultProductService productService;
	
	@Test
	public void findProductsWithoutRelations()
	{
		List<ProductModel> products = productService.findProductsWithoutRelations();		
		Assert.assertNotNull(products);
	}
	@Test
	public void findProductsWithImages()
	{
		List<ProductModel> products = productService.findProductsWithImages();		
		Assert.assertNotNull(products);
	}
	@Test
	public void findProductsWithRelatedProducts()
	{
		List<ProductModel> products = productService.findProductsWithRelatedProducts();		
		Assert.assertNotNull(products);
	}
	@Test
	public void findProductWithImages()
	{
		ProductModel products = productService.findProductWithImages(new Long(1));		
		Assert.assertNotNull(products);
	}
	@Test
	public void findProductWithRelatedProductsBy()
	{
		ProductModel products = productService.findProductWithRelatedProductsBy(new Long(1));		
		Assert.assertNotNull(products);
	}
	@Test
	public void findRelatedProductsBy()
	{
		List<ProductModel> products = productService.findRelatedProductsBy(new Long(1));		
		Assert.assertNotNull(products);
	}
	@Test
	public void findImageProductsBy()
	{
		List<ImageModel> images = productService.findImageProductsBy(new Long(1));		
		Assert.assertNotNull(images);
	}
	@Test
	public void findProductWithoutRelationsBy()
	{
		ProductModel products = productService.findProductWithoutRelationsBy(new Long(1));		
		Assert.assertNotNull(products);
	}
	
}


