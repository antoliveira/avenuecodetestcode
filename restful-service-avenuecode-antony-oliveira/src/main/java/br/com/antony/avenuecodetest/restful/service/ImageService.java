package br.com.antony.avenuecodetest.restful.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.antony.avenuecodetest.restful.model.ImageModel;
import br.com.antony.avenuecodetest.restful.model.ProductModel;
import br.com.antony.avenuecodetest.restful.service.GenericService;
/**
 * 
 * @author Antony Oliveira
 *
 */
public interface ImageService extends GenericService<ImageModel> {
	
}
