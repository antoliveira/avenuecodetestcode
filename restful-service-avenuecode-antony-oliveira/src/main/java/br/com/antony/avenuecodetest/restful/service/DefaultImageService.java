package br.com.antony.avenuecodetest.restful.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.antony.avenuecodetest.restful.model.ImageModel;
import br.com.antony.avenuecodetest.restful.model.ProductModel;
import br.com.antony.avenuecodetest.restful.repository.ImagePersistenceRepository;
import br.com.antony.avenuecodetest.restful.repository.ProductPersistenceRepository;

/**
 * 
 * @author Antony Oliveira
 *
 */
@Service
public class DefaultImageService implements ImageService {

	@Autowired
	private ImagePersistenceRepository imagePersistenceRepository;

	@Override
	public ImageModel save(ImageModel entity) {
		return imagePersistenceRepository.save(entity);
	}
	
	@Override
	public void delete(Long id) {
		imagePersistenceRepository.delete(id);
	}

	@Override
	public ImageModel getById(Long id) {
		return imagePersistenceRepository.getOne(id);
	}

	@Override
	public List<ImageModel> getAll() {
		return imagePersistenceRepository.findAll();
	}

}
