package br.com.antony.avenuecodetest.restful.endpoints;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.antony.avenuecodetest.restful.model.ImageModel;
import br.com.antony.avenuecodetest.restful.model.ProductModel;
import br.com.antony.avenuecodetest.restful.service.ImageService;
import br.com.antony.avenuecodetest.restful.service.ProductService;

/**
 * 
 * @author Antony Oliveira
 *
 */

@Component
@Path("/products")
public class ProductController {

	final static Logger logger = Logger.getLogger(ProductController.class);

	 @Autowired
	 ProductService productService;
	 
	 @Autowired
	 ImageService imageService;

	 @POST  
	 @Path("/save")  
	 @Produces(MediaType.APPLICATION_JSON)
	 public Response addProduct(ProductModel product)  
	 {  
	  productService.save(product);
      return Response.ok(product).build();  
	 }	
	 
	 @DELETE  
	 @Path("/delete/{id}")  
	 @Produces(MediaType.APPLICATION_JSON)  
	 public Response deleteProduct(@PathParam("id") Long id)  
	 {  
		 productService.delete(id);
		 return Response.ok("SUCCESS").build(); 
	 }  

	 @PUT  
	 @Path("/update")
	 @Produces(MediaType.APPLICATION_JSON)  
	 public Response updateProduct(ProductModel product)  
	 {  
	  productService.save(product);  
	  return Response.ok(product).build();  
	 }  
	 
	 @POST  
	 @Path("/img/save")  
	 @Produces(MediaType.APPLICATION_JSON)
	 public Response addImage(ImageModel image)  
	 {  
		 imageService.save(image);
      return Response.ok(image).build();  
	 }	
	 
	 @DELETE  
	 @Path("/img/delete/{id}")  
	 @Produces(MediaType.APPLICATION_JSON)  
	 public Response deleteImage(@PathParam("id") Long id)  
	 {  
		 imageService.delete(id);
		 return Response.ok("SUCCESS").build(); 
	 }  

	 @PUT  
	 @Path("/img/update")
	 @Produces(MediaType.APPLICATION_JSON)  
	 public Response updateImage(ImageModel image)  
	 {  
		 imageService.save(image);  
	  return Response.ok(image).build();  
	 }  

	
	@GET
	@Path("/prodWithoutRelations/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findProductWithoutRelationsBy(@PathParam("id") Long id) {
		ProductModel product = productService.findProductWithoutRelationsBy(id);
		if (product == null) {
			logger.debug("Product with id " + id + " does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found Product:: " + product);
		return Response.ok(product).build();
	}

	@GET
	@Path("/prodWithRelated/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findProductWithRelatedProductsBy(@PathParam("id") Long id) {
		ProductModel product = productService
				.findProductWithRelatedProductsBy(id);
		if (product == null) {
			logger.debug("Product with id " + id + " does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found Product:: " + product);
		return Response.ok(product).build();
	}

	@GET
	@Path("/prodWithImages/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findProductWithImages(@PathParam("id") Long id) {
		ProductModel product = productService.findProductWithImages(id);
		if (product == null) {
			logger.debug("Product with id " + id + " does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found Product:: " + product);
		return Response.ok(product).build();
	}

	@GET
	@Path("/getProductsEager")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProductsEager() {
		List<ProductModel> products = (List<ProductModel>) productService
				.getAll();
		if (products.isEmpty()) {
			logger.debug("Products does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found " + products.size() + " Products");
		logger.debug(Arrays.toString(products.toArray()));
		return Response.ok(products).build();
	}

	@GET
	@Path("/getWithImages")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findProductsWithImages() {
		List<ProductModel> products = (List<ProductModel>) productService
				.findProductsWithImages();
		if (products.isEmpty()) {
			logger.debug("Products does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found " + products.size() + " Products");
		logger.debug(Arrays.toString(products.toArray()));
		return Response.ok(products).build();
	}

	@GET
	@Path("/getWithRelatedProducts")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findProductsWithRelatedProducts() {
		List<ProductModel> products = (List<ProductModel>) productService
				.findProductsWithRelatedProducts();
		if (products.isEmpty()) {
			logger.debug("Products does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found " + products.size() + " Products");
		logger.debug(Arrays.toString(products.toArray()));
		return Response.ok(products).build();
	}

	@GET
	@Path("/findProductsWithoutRelations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findProductsWithoutRelations() {
		List<ProductModel> products = (List<ProductModel>) productService
				.findProductsWithoutRelations();
		if (products.isEmpty()) {
			logger.debug("Products does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found " + products.size() + " Products");
		logger.debug(Arrays.toString(products.toArray()));
		return Response.ok(products).build();
	}

	@GET
	@Path("/findImageProductsBy/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findImageProductsBy(@PathParam("id") Long id) {
		List<ImageModel> images = productService.findImageProductsBy(id);
		if (CollectionUtils.isEmpty(images)) {
			logger.debug("Product with id " + id + " does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found Product:: " + images);
		return Response.ok(images).build();
	}

	@GET
	@Path("/findRelatedProductsBy/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findRelatedProductsBy(@PathParam("id") Long id) {
		List<ProductModel> products = productService.findRelatedProductsBy(id);
		if (CollectionUtils.isEmpty(products)) {
			logger.debug("Product with id " + id + " does not exists");
			return Response.noContent().build();
		}
		logger.debug("Found Product:: " + products);
		return Response.ok(products).build();
	}

}
