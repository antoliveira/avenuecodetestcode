package br.com.antony.avenuecodetest.restful.model;

import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Antony Oliveira
 *
 */

@Entity
@Table(name = "product")
public class ProductModel implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "name", length = 50)
	private String name;

	@Column(name = "description", length = 50)
	private String description;
	
	@Transient
	private List<ProductModel> relatedProducts;
	
	@OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.PERSIST)
	@JoinColumn(name="IMAGE_ID")
	private Collection<ImageModel> images;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinTable(name="Parent_Child", joinColumns=@JoinColumn(name="childId"), inverseJoinColumns=@JoinColumn(name="parentId"))
	private ProductModel parent;
	
	public ProductModel() {
	}

	public ProductModel(Long id) {
		this.id = id;
	}

	public ProductModel(Long id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public ProductModel(String description, String name) {
		this.name = name;
		this.description = description;
	}
	
	public ProductModel(Long id, String name, String description,
			Collection<ImageModel> images, ProductModel parent) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.images = images;
		this.parent = parent;
	}
	
	public ProductModel(Long id, String name, String description,
			ProductModel parent) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.parent = parent;
	}
	
	public ProductModel(Long id, String name, String description,
			Collection<ImageModel> images) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.images = images;

	}
	
	public ProductModel(Long id, String name, String description,
			List<ProductModel> relatedProducts, Collection<ImageModel> images,
			ProductModel parent) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.relatedProducts = relatedProducts;
		this.images = images;
		this.parent = parent;
	}

	public List<ProductModel> getRelatedProducts() {
		return relatedProducts;
	}

	public void setRelatedProducts(List<ProductModel> relatedProducts) {
		this.relatedProducts = relatedProducts;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Collection<ImageModel> getImages() {
		return images;
	}

	public void setImages(Collection<ImageModel> images) {
		this.images = images;
	}
	
	public ProductModel getParent() {
		return parent;
	}

	public void setParent(ProductModel parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Id: ").append(this.id).append(", name: ").append(this.name).append(", description: ")
				.append(this.description);
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (id == null || obj == null || getClass() != obj.getClass())
			return false;
		ProductModel toCompare = (ProductModel) obj;
		return id.equals(toCompare.id);
	}

	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}

}
