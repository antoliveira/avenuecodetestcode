package br.com.antony.avenuecodetest.restful.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import br.com.antony.avenuecodetest.restful.model.ImageModel;
import br.com.antony.avenuecodetest.restful.model.ProductModel;
import br.com.antony.avenuecodetest.restful.service.GenericService;
/**
 * 
 * @author Antony Oliveira
 *
 */
public interface ProductService extends GenericService<ProductModel> {
	
	List<ProductModel> findProductsWithoutRelations();
	List<ProductModel> findProductsWithImages();
	List<ProductModel> findProductsWithRelatedProducts();
	ProductModel findProductWithImages(Long id);
	ProductModel findProductWithRelatedProductsBy(Long id);
	List<ProductModel> findRelatedProductsBy(Long id);
	List<ImageModel> findImageProductsBy(Long id);
	ProductModel findProductWithoutRelationsBy(Long id);
	
}
