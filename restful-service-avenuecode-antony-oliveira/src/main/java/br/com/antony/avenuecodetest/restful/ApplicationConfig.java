package br.com.antony.avenuecodetest.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
 
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
 

@Component
@ApplicationPath("/antonyrest")
public class ApplicationConfig extends ResourceConfig{

	 @Autowired
	  public ApplicationConfig(ObjectMapper objectMapper) {
	    packages("br.com.antony.avenuecodetest.restful");
	    register(new ObjectMapperContextResolver(objectMapper));
	  }
	 
	  @Provider
	  public static class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {
	 
	    private final ObjectMapper mapper;
	 
	    public ObjectMapperContextResolver(ObjectMapper mapper) {
	      this.mapper = mapper;
	    }
	 
	    @Override
	    public ObjectMapper getContext(Class<?> type) {
	      return mapper;
	    }
	  }
	
}
