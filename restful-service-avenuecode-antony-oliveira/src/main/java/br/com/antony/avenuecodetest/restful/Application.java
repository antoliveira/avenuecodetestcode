package br.com.antony.avenuecodetest.restful;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import br.com.antony.avenuecodetest.restful.model.ImageModel;
import br.com.antony.avenuecodetest.restful.model.ProductModel;
import br.com.antony.avenuecodetest.restful.service.DefaultProductService;

/**
 * 
 * @author Antony Oliveira
 *
 */

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
		DefaultProductService service = ctx.getBean(DefaultProductService.class);
		
		ProductModel prd1 = new ProductModel("Bota","bota preta masculina");
		ProductModel prd2 = new ProductModel("Camisa","Camisa vermelha gola em v");
		ProductModel prd3 = new ProductModel("Bermuda", "Bermuda camuflada tamanho g");
		ProductModel prd4 = new ProductModel("Boné","Boné de pescador em lona");
		
		ImageModel image1 = new ImageModel("png");
		ImageModel image2 = new ImageModel("tiff");
		ImageModel image3 = new ImageModel("jpg");
		

		prd3.setParent(prd1);
		prd4.setParent(prd2);
		
		List<ImageModel> imagesPrd1 = new ArrayList<ImageModel>();
		imagesPrd1.add(image1);
		imagesPrd1.add(image2);
		prd1.setImages(imagesPrd1);
		
		List<ImageModel> imagesPrd2 = new ArrayList<ImageModel>();
		imagesPrd1.add(image3);
		prd2.setImages(imagesPrd2);
		
		service.save(prd1);
		service.save(prd2);
		service.save(prd3);
		service.save(prd4);
		
	}
}
