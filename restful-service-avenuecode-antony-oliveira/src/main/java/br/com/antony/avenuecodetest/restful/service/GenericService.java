package br.com.antony.avenuecodetest.restful.service;

/**
 * @author Antony Oliveira
 */
import java.io.Serializable;
import java.util.List;

public interface GenericService<E> {

	E save(E entity);

	E getById(Long id);

	List<E> getAll();
	
	void delete(Long id);
}
