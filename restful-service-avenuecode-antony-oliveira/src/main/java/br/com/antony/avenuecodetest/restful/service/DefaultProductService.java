package br.com.antony.avenuecodetest.restful.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.antony.avenuecodetest.restful.model.ImageModel;
import br.com.antony.avenuecodetest.restful.model.ProductModel;
import br.com.antony.avenuecodetest.restful.repository.ImagePersistenceRepository;
import br.com.antony.avenuecodetest.restful.repository.ProductPersistenceRepository;

/**
 * 
 * @author Antony Oliveira
 *
 */
@Service
public class DefaultProductService implements ProductService {

	@Autowired
	private ProductPersistenceRepository productPersistenceRepository;
	@Autowired
	private ImagePersistenceRepository imagePersistenceRepository;

	@Override
	public ProductModel save(ProductModel entity) {
		return productPersistenceRepository.save(entity);
	}
	
	@Override
	public void delete(Long id) {
		productPersistenceRepository.delete(id);
	}

	@Override
	public ProductModel getById(Long id) {
		return productPersistenceRepository.findOne((Long) id);
	}

	@Override
	public List<ProductModel> getAll() {
		return productPersistenceRepository.findAll();
	}

	@Override
	public List<ProductModel> findProductsWithoutRelations() {
		return productPersistenceRepository.findProductsWithoutRelations();
	}

	@Override
	public List<ProductModel> findProductsWithImages() {
		List<ProductModel> prds = productPersistenceRepository.findProductsWithoutRelations();		
		for (ProductModel product : prds) {
			product.setImages(this.findImageProductsBy(product.getId()));
		}
		return prds;
	}

	@Override
	public List<ProductModel> findProductsWithRelatedProducts() {
		List<ProductModel> prds = productPersistenceRepository.findProductsWithoutRelations();
		
		for (ProductModel product : prds) {
			product.setRelatedProducts(this.findRelatedProductsBy(product.getId()));
		}
		return prds;
	}

	@Override
	public ProductModel findProductWithImages(Long id) {
		ProductModel product = productPersistenceRepository.findProductWithoutRelationsBy((Long) id);
		product.setImages(imagePersistenceRepository.findImageProductsBy(product.getId()));
		return product;
	}

	@Override
	public ProductModel findProductWithRelatedProductsBy(Long id) {
		ProductModel product = productPersistenceRepository.findProductWithoutRelationsBy((Long) id);
		product.setRelatedProducts(this.findRelatedProductsBy(product.getId()));
		return product;
	}

	@Override
	public List<ProductModel> findRelatedProductsBy(Long id) {
		return productPersistenceRepository.findRelatedProductsBy(id);
	}

	@Override
	public List<ImageModel> findImageProductsBy(Long id) {		
		return imagePersistenceRepository.findImageProductsBy(id);
	}

	@Override
	public ProductModel findProductWithoutRelationsBy(Long id) {
		return productPersistenceRepository.findProductWithoutRelationsBy(id);
	}

}
