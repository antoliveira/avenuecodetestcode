package br.com.antony.avenuecodetest.restful.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.antony.avenuecodetest.restful.model.ImageModel;
import br.com.antony.avenuecodetest.restful.model.ProductModel;
/**
 * 
 * @author Antony Oliveira
 *
 */

@Repository
public interface ImagePersistenceRepository extends JpaRepository<ImageModel, Long> {	
	@Query("SELECT p.images FROM ProductModel p where p.id = :pk ")
	List<ImageModel> findImageProductsBy(@Param("pk") Long id);	
}
