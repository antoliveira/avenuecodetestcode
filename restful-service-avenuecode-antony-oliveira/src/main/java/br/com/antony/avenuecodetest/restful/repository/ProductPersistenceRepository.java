package br.com.antony.avenuecodetest.restful.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.antony.avenuecodetest.restful.model.ProductModel;
/**
 * 
 * @author Antony Oliveira
 *
 */

@Repository
public interface ProductPersistenceRepository extends JpaRepository<ProductModel, Long> {

	@Query("SELECT new br.com.antony.avenuecodetest.restful.model.ProductModel(p.id, p.name, p.description) FROM ProductModel p") 
	List<ProductModel> findProductsWithoutRelations();
	
	@Query("SELECT new br.com.antony.avenuecodetest.restful.model.ProductModel(p.id, p.name, p.description) FROM ProductModel p where p.id = :id") 
	ProductModel findProductWithoutRelationsBy(@Param("id")Long id);
	
	
	@Query("SELECT new br.com.antony.avenuecodetest.restful.model.ProductModel(p.id, p.name, p.description) FROM ProductModel p where p.parent.id = :id")
	List<ProductModel> findRelatedProductsBy(@Param("id") Long id);
	
	
}
