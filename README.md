# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Avenue code java test
* 1.0


### How do I get set up? ###

* Summary of set up
Clone repository
* Configuration
n/a
* Dependencies
oracle java 1.8
* Database configuration
n/a
* How to run tests
run mvn test
* Deployment instructions
run mvn spring-boot:run

### Contribution guidelines ###

* services calls
the products id are sequential.

prd1 = id 1
prd2 = id 2
prd3 = id 3
prd4 = id 4

base url "localhost:8080/antonyrest/products

To use those url's, you need to use POSTMAN or other similar software.

/save
Type post
create a new product, you need put id in product
example:
 {
    "id": PUT ID HERE,
    "name": "bota preta masculina",
    "description": "Bota",
    "relatedProducts": null,<< trnasient field
    "images": [
      {
        "id": 1,
        "type": "png"
      },
      {
        "id": 2,
        "type": "tiff"
      },
      {
        "id": 3,
        "type": "jpg"
      }
    ],
    "parent": null
  }

/update
Type PUT
update a product, you need to provide the id to update a product
example: 
{
    "id": PUT A VALID ID HERE,
    "name": "bota preta masculina",
    "description": "Bota",
    "relatedProducts": null,<< trnasient field
    "images": [
      {
        "id": 1,
        "type": "png"
      },
      {
        "id": 2,
        "type": "tiff"
      },
      {
        "id": 3,
        "type": "jpg"
      }
    ],
    "parent": null
  }

/delete/{id}
type DELETE
delte product from database using ID
example
/delete/1

/img/save
Save a image in database, you need to provide a ID
type POST
example
      {
        "id": PUT ID HERE,
        "type": "png"
      }

/img/update
Update image in database, you need to provide a valid ID
type PUT
example
      {
        "id": PUT VALID ID HERE,
        "type": "png"
      }

/img/delete/{id}
Delete image from database, you need to provide a valid ID
type DELETE
example:
/img/delete/4


All the methods below use request type GET

/prodWithoutRelations/{id}
return a product instance by id without relationships

/prodWithRelated/{id}
return a product instance by id with related (childs) products

/prodWithImages/{id}
return a product instance by id with all images

/getProductsEager
return all products with all relations

/getWithImages
return all products with related images

/getWithRelatedProducts
return all products with all related (childs) products

/findProductsWithoutRelations
return all products without relations

/findImageProductsBy/{id}
return all images related a specific product by id

/findRelatedProductsBy/{id}
return all related (child) products related a specific product by id

### Who do I talk to? ###
Bellow the initial data for tests, has 4 products created 

prd1 = id 1
prd2 = id 2
prd3 = id 3
prd4 = id 4


prd1 have 2 images
prd2 have 1 image
prd3 is related (child) of prd1 and have no images
prd4 is related (child) of prd2 and have no images


                ProductModel prd1 = new ProductModel("Bota","bota preta masculina");
		ProductModel prd2 = new ProductModel("Camisa","Camisa vermelha gola em v");
		ProductModel prd3 = new ProductModel("Bermuda", "Bermuda camuflada tamanho g");
		ProductModel prd4 = new ProductModel("Boné","Boné de pescador em lona");
		
		ImageModel image1 = new ImageModel("png");
		ImageModel image2 = new ImageModel("tiff");
		ImageModel image3 = new ImageModel("jpg");
		

		prd3.setParent(prd1);
		prd4.setParent(prd2);
		
		List<ImageModel> imagesPrd1 = new ArrayList<ImageModel>();
		imagesPrd1.add(image1);
		imagesPrd1.add(image2);
		prd1.setImages(imagesPrd1);
		
		List<ImageModel> imagesPrd2 = new ArrayList<ImageModel>();
		imagesPrd1.add(image3);
		prd2.setImages(imagesPrd2);


####################################################################################

* Repo owner or admin
* Other community or team contact

The tests are not working fine, a had no time to do all the tests.
I am in the final phase of the project and this way my time is very short, I was able to do the test now at dawn.